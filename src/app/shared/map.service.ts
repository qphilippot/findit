import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class MapService {
  // Observable string sources
  private coordinatesSource = new Subject<{lat: number, long: number}>();

  constructor(){}
  // Observable string streams
  coordinates$ = this.coordinatesSource.asObservable();

  // Service message commands
  updateCoordinates(coordinatesSource: {lat: number, long: number}) {
    this.coordinatesSource.next(coordinatesSource);
  }
}
