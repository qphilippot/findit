export class Result {
  public static instances: Result[] = [];


  public recordid: string;
  public record_timestamp: string;
  public geometry: {
    coordinates: Array<Number>[2],
    type: string
  };

  public fields;
  public isHover = false;
  public isActive = false;


  public defaultIcon = {
    url: 'https://s3.eu-west-3.amazonaws.com/qphilippot-find-it/assets/img/bleu.png',
    scaledSize: {
      width: 20,
      height: 20
    }
  };

  public focusIcon = {
    url: 'https://s3.eu-west-3.amazonaws.com/qphilippot-find-it/assets/img/rouge.png',
    scaledSize: {
      width: 25,
      height: 25
    }
  };

  public icon;

  constructor(data) {
    this.recordid = data.recordid;
    this.record_timestamp = data.record_timestamp;
    this.geometry = data.geometry;
    this.fields = data.fields;

    this.icon = this.defaultIcon;
  }

  get lat() {
    return this.geometry.coordinates[1];
  }

  get long() {
    return this.geometry.coordinates[0];
  }

  get date() {
    const record_date = new Date(this.record_timestamp);

    return [
      record_date.getDate() < 10 ? '0' +  record_date.getDate() : record_date.getDate(),
      record_date.getMonth() + 1 < 10 ? '0' +  (record_date.getMonth() + 1) : record_date.getMonth() + 1,
      record_date.getFullYear()
    ].join('/');
  }

  get fieldsTags() {
    return Object.keys(this.fields);
  }

  inactive() {
    this.isActive = false;
    this.icon = this.defaultIcon;
  }

  active() {
    this.isActive = true;

    this.icon = this.focusIcon;
  }

  enter() {
    this.isHover = true;

    this.icon = this.focusIcon;
  }

  leave() {
    this.isHover = false;

    this.icon = this.defaultIcon;
  }
}
