import { Result } from './result.model';
import { Injectable } from '@angular/core';

@Injectable()
export class ResultService {
  constructor(){}

  addResult(data) {
    const result = new Result(data);
    Result.instances.push(result);
  }

  clearResults() {
    Result.instances.length = 0;
  }

  inactiveAll() {
    const results = Result.instances;
    results.forEach(result => result.inactive());
  }

  active(result: Result) {
    this.inactiveAll();
    result.active();
  }
}
