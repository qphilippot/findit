import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';

import { SearchService } from './search.service';
import { config } from './search-bar.model';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})

export class SearchBarComponent implements OnInit {
  @ViewChild('constraintSecteurEnabled') constraintSecteur;
  @ViewChild('constraintActivityEnabled') constraintActivity;
  @Output() onSearchEnd = new EventEmitter();

  timeSearch: string;
  
  public basicInput = false;
  public data;
  public defaultActivitiesCode;
  public defaultActivitiesName;
  public defaultDepertement: string[] = ['84', '26', '30', '13', '07'];

  public static timestamp_4weeks : number = 4 * 7 * 24 * 60 * 60 * 1000;

  constructor(
    public searchService: SearchService
  ) { }

  ngOnInit() {
    this.data = config;
    this.defaultActivitiesCode = [].concat(
      this.searchService.getMainActivityCodeByCodeActivityGroup('86'),
      this.searchService.getMainActivityCodeByCodeActivityGroup('75')
    );

    this.defaultActivitiesName = this.defaultActivitiesCode.map(code => {
      return this.data.apet700_label[code];
    });
  }

  onSubmit(form: NgForm) {
    // console.log(this.searchService.getMainActivityCodeByCodeActivityGroup('86'));
    // console.log(this.searchService.getMainActivityCodeByCodeActivityGroup('32'));
    const constraints = [
      // this.getDepetConstraintString(form.value.depets),
      this.getDepetConstraintString(this.defaultDepertement),
      '(' + this.getApet700Constraints(this.defaultActivitiesCode) + ')',
       + this.getTodayConstraint()
    ];

    const constraintsString = constraints.join('AND') + '&rows=100&sort=record_timestamp ';


    this.searchService.search(constraintsString).subscribe((data) => {

    });

  }

  searchDefault() {
    const constraints = [
      // this.getDepetConstraintString(form.value.depets),
      
    ];

    switch(this.timeSearch) {
      case 'today': 
        //constraints.push('(' + this.getTodayConstraint() + ')');
        constraints.push(encodeURIComponent('('+this.timeConstraints(this.today, this.today) + ')'));
        break;
      case 'weeks': 
        // constraints.push('(' + this.get4NextWeeksContraints() + ')');

        constraints.push(encodeURIComponent('('+this.timeConstraints(this.today, this.next4Weeks) + ')'));
        break;
      case 'last_weeks': 
        // constraints.push('(' + this.get4LastWeeksContraints() + ')');

        constraints.push(encodeURIComponent('('+this.timeConstraints(this.last4Weeks, this.today) + ')'));
        break;
      default:
        //constraints.push('(' + this.getTodayConstraint() + ')');
        break;
    }
   
    console.log('is checked  secture ? ', this.constraintSecteur.nativeElement.checked, this.constraintActivity.nativeElement.checked)

    if (this.constraintSecteur.nativeElement.checked === true) {
      constraints.push('(' + this.getDepetConstraintString(this.defaultDepertement) + ')');
    }

    if (this.constraintActivity.nativeElement.checked === true) {
      constraints.push('(' + this.getApet700Constraints(this.defaultActivitiesCode) + ')');
    }


    console.log(constraints);

    const constraintsString = constraints.join('AND') + '&rows=100&sort=record_timestamp ';
  
    console.log('str', constraintsString);
    this.searchService.search(constraintsString).subscribe((data) => {
      // console.log(Result.instances);
      // console.log('-- search end --');
      this.onSearchEnd.emit(null);
    });
  }

  getApet700Constraints(codes: String[]) {
    return this.searchService.getApet700ConstraintsString(codes);
  }


  showBasicInput() {
    this.basicInput = true;
  }

  get today() {
    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month = (currentDate.getMonth() + 1 < 10) ? ('0' + (currentDate.getMonth() + 1)) : (currentDate.getMonth() + 1);
    const day = (currentDate.getDate() < 10 ? '0' + currentDate.getDate() : currentDate.getDate());
    return year + '-' + month + '-' + day;
  }

  get next4Weeks() {
    const currentDate = new Date();
    const nextWeek = new Date(currentDate.getTime() + SearchBarComponent.timestamp_4weeks);
    return SearchBarComponent.timestampToSIRENDate(nextWeek);
  }

  static timestampToSIRENDate(date) { 
    const year = date.getFullYear();
    const month = (date.getMonth() + 1 < 10) ? ('0' + (date.getMonth() + 1)) : (date.getMonth() + 1);
    const day = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
    return year + '-' + month + '-' + day;
  }

  get last4Weeks() {
    const currentDate = new Date();
    const lastWeek = new Date(currentDate.getTime() - SearchBarComponent.timestamp_4weeks);
    return SearchBarComponent.timestampToSIRENDate(lastWeek);
  }

  getTodayConstraint() {
    return '(dcren%3D' + this.today + ')';
  }

  get4NextWeeksContraints() {
    return '(dcren%3E%3D' + this.today + ' AND dcren%3C%3D' + this.next4Weeks +')';
  }

  get4LastWeeksContraints() {
    return '(dcren%3C%3D' + this.today + ' AND dcren%3E%3D' + this.last4Weeks +')';
  }


  timeConstraints(minTimestamp, maxTimestamp) {
    const tags = [
      'dcren',
      'amintren',
      'dcret',
      'ddebact',
      'datamaj',
      'record_timestamp'
    ];

    return tags.map(tag => {
      return `(${tag}<=${maxTimestamp} AND ${tag}>=${minTimestamp})`;
    }).join(' OR ');
  }

  getDepetConstraintString(depets: String[]) {
    if (depets.length === 0) {
      return '';
    }

    else {
      const rules = depets
        .map(departement => 'depet=' + departement)
        .join(' OR ');
      return '(' + rules + ')';
    }
  }

}
