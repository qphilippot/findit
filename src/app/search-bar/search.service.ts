import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from './search-bar.model';

import { map } from 'rxjs/operators';
import { ResultService } from '../shared/result.service';

@Injectable()
export class SearchService {
  constructor(
    private http: HttpClient,
    private resultService: ResultService
  ){

  }

  getMainActivityCodeByCodeActivityGroup(codeGroup) {
    return config.apet700_codes_by_group[codeGroup];
  }

   search(params: string) {
    this.resultService.clearResults();
    //console.log(config.url + params);
    return this.http.get<{
      records: Object[]
    }>(config.url + params).pipe(
      map((raw) => {
        //console.log(raw);
        if (raw.records.length === 0) {
          alert('aucun résultat, réessayez demain');
        }

        raw.records.map((record) => {
          return this.resultService.addResult(record);
        });
      })
    );
  }

  getApet700ConstraintsString(codes: String[]) {
    return codes.map((code) => {
      return `(apet700%3D${code}')`;
    }).join('OR');
  }
}
