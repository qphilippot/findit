import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {MatRadioModule} from '@angular/material/radio';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule } from '@agm/core';

import { AppComponent } from './app.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { ResultListComponent } from './result/result-list/result-list.component';
import { ResultItemComponent } from './result/result-item/result-item.component';
import { HeaderComponent } from './header/header.component';

import { SearchService } from './search-bar/search.service';
import { ResultService } from './shared/result.service';
import { MapService } from './shared/map.service';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { MenuButtonComponent } from './header/menu-button/menu-button.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchBarComponent,
    ResultListComponent,
    ResultItemComponent,
    HeaderComponent,
    NavBarComponent,
    MenuButtonComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    MatRadioModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDNs1qfSL4sURTeJZijSx6SPZOedsM02Uw'
    })
  ],
  providers: [
    SearchService,
    ResultService,
    MapService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
