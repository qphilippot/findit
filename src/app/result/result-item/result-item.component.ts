import { Component, OnInit, Input } from '@angular/core';
import { Result } from '../../shared/result.model';
import { ResultService } from '../../shared/result.service';
import { MapService } from '../../shared/map.service';


@Component({
  selector: 'app-result-item',
  templateUrl: './result-item.component.html',
  styleUrls: ['./result-item.component.css']
})

export class ResultItemComponent implements OnInit {
  @Input() result: Result;

  constructor(
    public resultService: ResultService,
    public mapService: MapService
  ) { }

  ngOnInit() {
  }

  showMarker() {
    this.result.enter();
    // this.mapService.updateCoordinates({
    //   lat: this.result.lat,
    //   long: this.result.long
    // });
  }

  leaveMarker() {
    this.result.leave();
    // this.mapService.updateCoordinates({
    //   lat: this.result.lat,
    //   long: this.result.long
    // });
  }

  activeMarker() {
    this.resultService.active(this.result);
    // this.mapService.updateCoordinates({
    //   lat: this.result.lat,
    //   long: this.result.long
    // });
  }

  prettyDate(date: string) {
    const newDate = date.split('-');
    return newDate[2] + '/' + newDate[1] + '/' + newDate[0];
  }
}
