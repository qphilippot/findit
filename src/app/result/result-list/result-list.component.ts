import { Component, OnInit } from '@angular/core';
import { Result } from '../../shared/result.model';

@Component({
  selector: 'app-result-list',
  templateUrl: './result-list.component.html',
  styleUrls: ['./result-list.component.css']
})
export class ResultListComponent implements OnInit {
  public results = Result.instances;

  constructor() { }

  ngOnInit() {
  }

}
