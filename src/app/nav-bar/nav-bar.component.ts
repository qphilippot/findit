import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Result } from '../shared/result.model';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  @Output('openSearch') openSearchSignal = new EventEmitter();
  public results = Result.instances;
  constructor() { }

  ngOnInit() {
  }

  openSearch() {
    console.log('nav bar trigger');
    this.openSearchSignal.emit(null);
  }


}
