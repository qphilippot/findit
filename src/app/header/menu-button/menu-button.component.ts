import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-menu-button',
  templateUrl: './menu-button.component.html',
  styleUrls: ['./menu-button.component.css']
})
export class MenuButtonComponent implements OnInit {
  @Output() toggle: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  onClick() {
    this.toggle.emit(null); 
  }
}
