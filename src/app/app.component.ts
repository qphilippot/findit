import { Component, OnInit } from '@angular/core';
import { Result } from './shared/result.model';
import { MapService } from './shared/map.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  public coordinates;
  public maskMap = true;
  public results = Result.instances;
  public navbarIsOpen: boolean = false;
  public searchIsVisible = true;

  constructor(
    public mapService: MapService
  ) {
    this.coordinates = {
      lat: 33,
      long: 34
    };

    // this.mapService.coordinates$.subscribe((coordinates) => {
    //   //this.coordinates = coordinates;
    // });
  }

  ngOnInit(): void {
    try {
      navigator.geolocation.getCurrentPosition((pos) => {
        this.coordinates = {
          lat: pos.coords.latitude,
          long: pos.coords.longitude
        };
      });
    }

    catch (error) {
      console.log(error);
      this.coordinates = {
        lat: 33,
        long: 34
      };
    }
  }

  updateMapContext(center) {
    this.coordinates.lat = center.lat;
    this.coordinates.long = center.long;
  }

  openNavBar() {
    this.navbarIsOpen = true;
  }

  closeNavBar() {
    this.navbarIsOpen = false;
  }

  toggleNavBar() {
    console.log('toggleNavBar');
    this.navbarIsOpen = !this.navbarIsOpen;
  }

  closeSearch() {
    this.searchIsVisible = false;
  }

  openSearch() {
    this.searchIsVisible = true;
  }
  onSearchEnd() {
    this.openNavBar();
    this.closeSearch();
  }
}
